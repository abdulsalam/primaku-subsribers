import { Box, Grid, useTheme } from "@mui/material";
import SaaSCard from "components/Dashboards/saas/Card";
import useTitle from "hooks/useTitle";
import EarningIcon from "icons/EarningIcon";
import PeopleIcon from "icons/PeopleIcon";
import { FC } from "react";

const SaaS: FC = () => {
  // change navbar title
  useTitle("PrimaKu");

  const theme = useTheme();

  const cardList = [
    {
      price: "On",
      Icon: PeopleIcon,
      title: "User Subscription",
      color: theme.palette.primary.main,
      link: '/dashboard/user-list',
    },
    {
      price: "98%",
      title: "Performance",
      Icon: EarningIcon,
      color: theme.palette.primary.purple,
      link: '/dashboard',
    },
  ];

  return (
    <Box pt={2} pb={4}>
      <Grid container spacing={{ xs: 2, sm: 3, md: 4 }}>
        {cardList.map((card, index) => (
          <Grid item lg={3} xs={6} key={index}>
            <SaaSCard card={card} />
          </Grid>
        ))}
      </Grid>
    </Box>
  );
};

export default SaaS;
