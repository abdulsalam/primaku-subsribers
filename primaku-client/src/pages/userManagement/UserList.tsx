import { Box, Button, styled } from "@mui/material";
import FlexBox from "components/FlexBox";
import SearchInput from "components/SearchInput";
import UserListColumnShape from "components/userManagement/columnShape";
import CustomTable from "components/userManagement/CustomTable";
import { UserState } from "components/userManagement/fakeData";
import useTitle from "hooks/useTitle";
import { FC, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { io } from "socket.io-client";
import { API } from "../../constants";

// socket
const socket = io(API.BASE_SOCKET);
// styled component
const StyledFlexBox = styled(FlexBox)(({ theme }) => ({
  justifyContent: "space-between",
  alignItems: "center",
  flexWrap: "wrap",
  marginBottom: 20,
  [theme.breakpoints.down(500)]: {
    width: "100%",
    "& .MuiInputBase-root": { maxWidth: "100%" },
    "& .MuiButton-root": {
      width: "100%",
      marginTop: 15,
    },
  },
}));

const UserList: FC = () => {
  // change navbar title
  useTitle("User List");

  const navigate = useNavigate();
  const handleAddUser = () => navigate("/dashboard/add-user");
  const [isConnected, setIsConnected] = useState(socket.connected);
  const [user, setUser] = useState<UserState[]>([]);
  const [exist, setExist] = useState(false);

  // listener on socket
  useEffect(() => {
    socket.on('connect', () => setIsConnected(true));
    socket.on('disconnect', () => setIsConnected(false));
    socket.on(API.BASE_CHANNEL, (data) => {
      const userCurrent = user;

      userCurrent.push(data)
      setUser(userCurrent);
    });
    socket.emit(API.BASE_CHANNEL, { data: 'sent!' });

    setInterval(() => {
      user.length > 0 && setExist(true)
    }, 3000)
    return () => {
      socket.off('connect');
      socket.off('disconnect');
      socket.off(API.BASE_CHANNEL);
    }
  }, [user])

  return (
    <>
      {exist &&
        <Box pt={2} pb={4}>
          <StyledFlexBox>
            <SearchInput placeholder="Search user..." />
            <Button variant="contained" onClick={handleAddUser}>
              Total: {user.length}
            </Button>
          </StyledFlexBox>

          <CustomTable columnShape={UserListColumnShape} data={user} />
        </Box>
      }
    </>
  );
};

export default UserList;
