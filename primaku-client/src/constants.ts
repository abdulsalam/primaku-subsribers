export const THEMES = {
  LIGHT: "light",
  DARK: "dark",
};

export const API = {
  BASE_URL: 'http://localhost:3000/api',
  BASE_SOCKET: 'ws://localhost:3005',
  BASE_CHANNEL: 'eventMemberUsers',
}