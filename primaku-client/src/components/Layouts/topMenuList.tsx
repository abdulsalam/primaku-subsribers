import Icons from "icons/sidebar";

const index = [
  {
    title: "Dashboard",
    Icon: Icons.DashboardIcon,
    path: "/dashboard",
  },
  {
    title: "User List",
    Icon: Icons.UserManagementIcon,
    path: "/dashboard/user-list",
  }
];

export default index;
