import { alpha, Badge, Box, Card, styled } from "@mui/material";
import { H3, H5 } from "components/Typography";
import { FC } from "react";
import { useNavigate } from "react-router-dom";

// root component interface
interface SaaSCardProps {
  card: any;
}

const StyledCard = styled(Card)(({ theme }) => ({
  padding: "2rem 1.5rem",
  display: "flex",
  alignItems: "center",
  height: "100%",
  [theme.breakpoints.down("sm")]: {
    padding: "1.5rem",
    flexDirection: "column",
    justifyContent: "center",
    "& .MuiBox-root": {
      marginRight: 0,
      textAlign: "center",
    },
  },
}));

const SaaSCard: FC<SaaSCardProps> = ({ card }) => {
  const { Icon, title, color, price, link } = card;
  const navigate = useNavigate();

  return (
    <StyledCard onClick={() => navigate(link)}>
      <Box
        sx={{
          width: 60,
          height: 60,
          marginRight: 2,
          display: "flex",
          borderRadius: "50%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: alpha(color, 0.2),
        }}
      >
        <Icon sx={{ color }} />
      </Box>
      <Box mt={{ xs: "1rem", sm: 0 }}>
        <H5 color="text.disabled">{title}</H5>
        <Badge
          overlap="circular"
          variant="dot"
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          sx={{
            "& .MuiBadge-badge": {
              width: 11,
              height: 11,
              right: "7%",
              borderRadius: "50%",
              border: "2px solid #fff",
              backgroundColor: "success.main",
            },
          }}
        >
          <H3>{price}</H3>
        </Badge>
      </Box>
    </StyledCard>
  );
};

export default SaaSCard;
