// user type
export type UserState = {
  id: number;
  name: string;
  email: string;
  role: string;
}
export const initialUser: UserState[] = [
  {
    id: 0,
    email: "",
    name: "",
    role: "",
  }
]

export const userListFakeData = [
  {
    avatar: "/static/avatar/001-man.svg",
  },
  {
    avatar: "/static/avatar/003-boy.svg",
  },
  {
    avatar: "/static/avatar/011-man-2.svg",
  },
  {
    avatar: "/static/avatar/014-man-3.svg",
  },
  {
    avatar: "/static/avatar/018-boy-3.svg",
  },
  {
    avatar: "/static/avatar/020-man-4.svg",
  },
  {
    avatar: "/static/avatar/023-man-6.svg",
  },
];
