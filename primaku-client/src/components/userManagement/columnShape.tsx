import FlexBox from "components/FlexBox";
import { H6, Small, Tiny } from "components/Typography";
import UkoAvatar from "components/UkoAvatar";
import { userListFakeData } from "./fakeData";

const UserListColumnShape = [
  {
    Header: "Name",
    accessor: "name",
    minWidth: 200,
    Cell: ({ value }: any) => {
      const name = value;
      return (
        <FlexBox alignItems="center">
          <UkoAvatar src={userListFakeData[Math.floor(Math.random() * userListFakeData.length)].avatar} />
          <FlexBox flexDirection="column" ml={1}>
            <H6 color="text.primary">{name}</H6>
            <Tiny color="text.disabled">{'Primaku User Client'}</Tiny>
          </FlexBox>
        </FlexBox>
      );
    },
  },
  {
    Header: "Email",
    accessor: "email",
    minWidth: 150,
  },
  {
    Header: "Role",
    accessor: "role",
    minWidth: 200,
    Cell: ({ value }: any) => (
      <Small
        sx={{
          borderRadius: 10,
          padding: ".2rem 1rem",
          color: "background.paper",
          backgroundColor: "#A798FF",
        }}
      >
        {value}
      </Small>
    ),
  },
];

export default UserListColumnShape;
