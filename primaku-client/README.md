# PrimaKu Client

A Client App for user auth with atomic pattern

## Setup
- Make sure the [API](../primaku-api/) already run

## How to Run
- Open terminal and type `docker-compose up` (for run Client)
- Open browser in `localhost:3012`

## Account for Login based on Seeder
- email: admin@primaku.com
- password: #Admin123

## Documentation

## Others
- There's database backup on `src/database/primaku.sql` that can be an alternative for import method
- Type `docker-compose down` for stop the container of Client
- Client run on port `:3012`