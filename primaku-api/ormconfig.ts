import { User } from "./src/modules/users/entities/user.entity";

module.exports = {
    name: "default",
    type: process.env.DATABASE_DRIVER,
    host: process.env.DATABASE_HOST,
    port: process.env.DATABASE_PORT,
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    entities: [User],
    seeds: ['src/database/seeders/**/*{.ts,.js}'],
    factories: [],
}