# PrimaKu API

A Server App for user auth with modular pattern

## Setup
- Copy `.env.example` as `.env`
- Adjust the database changes on `.env` (Make sure the database already created on **MySQL**)

## How to Run
- Open terminal and type `docker-compose up` (for run API)
- The migration of `users` will automatic running & created
- On Another terminal, type `npm run db:seed` (for run seeder)

## Account for Login based on Seeder
- email: admin@primaku.com
- password: #Admin123

## Documentation
- Visit `localhost:3000/api` for Swagger Documentation
- [Postman Collection](https://api.postman.com/collections/1535500-c880ac28-9610-4a82-b11c-fd760faa3748?access_key=PMAT-01GTKXRSSEDT0ZMXKTPF3XMJ5P) - Need Import

## Others
- There's database backup on `src/database/primaku.sql` that can be an alternative for import method
- Type `docker-compose down` for stop the container of API
- API run on port `:3000`
- WebSocket run on port `:3005`