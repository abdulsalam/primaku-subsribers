import { MigrationInterface, QueryRunner, Table } from "typeorm"
import { TABLE } from "../../config/constant";

export class UserTable1677509672568 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: TABLE.USER.NAME,
            columns: [
                {
                    name: TABLE.USER.COLUMNS.ID,
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                },
                {
                    name: TABLE.USER.COLUMNS.NAME,
                    type: 'varchar',
                    length: `${TABLE.USER.MAXLENGTH}`,
                },
                {
                    name: TABLE.USER.COLUMNS.EMAIL,
                    type: 'varchar',
                    length: `${TABLE.USER.MAXLENGTH}`,
                    isUnique: true,
                },
                {
                    name: TABLE.USER.COLUMNS.PASSWORD,
                    type: 'varchar'
                },
                {
                    name: TABLE.USER.COLUMNS.ROLE,
                    type: 'enum',
                    enum: ['admin', 'member'],
                }
            ]
        }), true, false, false);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('users', true, true, true);
    }

}
