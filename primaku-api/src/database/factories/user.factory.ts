import { User } from '../../modules/users/entities/user.entity';
import { Faker } from "@faker-js/faker";
import { define } from 'typeorm-seeding';

define(User, (faker: typeof Faker) => {
  const user = new User();
  return user;
});