import { Factory, Seeder } from 'typeorm-seeding'
import { Connection } from 'typeorm'
import { User } from '../../modules/users/entities/user.entity'
import * as bcrypt from 'bcrypt';
import { saltOrRounds } from '../../config/constant';
import { TypeUser } from '../../modules/users/entities/member.entity';

export default class CreateUsers implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<any> {
        const password = await bcrypt.hash('#Admin123', saltOrRounds);
        await connection
            .createQueryBuilder()
            .insert()
            .into(User)
            .values([
                { name: 'Administrator', email: 'admin@primaku.com', password, role: TypeUser.ADMIN },
            ])
            .execute()
    }
}