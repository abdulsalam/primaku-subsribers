import { Injectable } from "@nestjs/common";

@Injectable()
export class Validator {
    private static validator: Validator;

    static getInstance() {
        if (this.validator === null) {
            this.validator = new Validator();
        }
        
        return this.validator;
    }

    notValid(params) {
        const missingValidation = [null, undefined, "", -1];

        return params.some((__v) => {
            return missingValidation.includes(__v);
        })
    }
}