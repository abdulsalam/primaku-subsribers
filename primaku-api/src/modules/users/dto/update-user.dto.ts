import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsInt, IsEmail, IsNotEmpty } from 'class-validator';
import { TypeUser } from '../entities/member.entity';

export class UpdateUserDto extends PartialType(CreateUserDto) {

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @ApiProperty({
        enum: Object.values(TypeUser)
    })
    @IsString()
    @IsNotEmpty()
    role: TypeUser;
}

export class UpdateUserPasswordDto {

    @ApiProperty({ required: true })
    @IsInt()
    @IsNotEmpty()
    id: number;

    @ApiProperty({ required: true })
    @IsString()
    @IsNotEmpty()
    password: string;

    @ApiProperty({ required: true })
    @IsString()
    oldPassword: string;
}