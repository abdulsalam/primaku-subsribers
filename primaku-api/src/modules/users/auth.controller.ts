import { Controller, Post, Body, Put, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { JwtAuthGuard } from '../../guard/jwt-auth.guard';
import { UpdateUserPasswordDto } from './dto/update-user.dto';
import { LoginUserDto } from './dto/login-user.dto';

@Controller()
export class AuthController {
  constructor(private readonly usersService: UsersService) {}

  @Post('login')
  create(@Body() loginUserDto: LoginUserDto) {
    return this.usersService.login(loginUserDto);
  }

  @UseGuards(JwtAuthGuard)
  @Put('password')
  update(@Body() updateUserPasswordDto: UpdateUserPasswordDto) {
    return this.usersService.password(updateUserPasswordDto);
  }
}
