import { TABLE } from "../../../config/constant";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { TypeUser } from './member.entity';

@Entity(TABLE.USER.NAME)
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: TABLE.USER.MAXLENGTH })
    name: string;

    @Column({ length: 255, unique: true })
    email: string;

    @Column({ name: TABLE.USER.COLUMNS.PASSWORD })
    password: string;

    @Column( { name: TABLE.USER.COLUMNS.ROLE })
    role: TypeUser
}
