import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ApiBody, ApiParam } from '@nestjs/swagger';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto, UpdateUserPasswordDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { LoginUserDto } from './dto/login-user.dto';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    private jwtService: JwtService
  ) {}

  @ApiBody({ type: CreateUserDto })
  async create(createUserDto: CreateUserDto) {
    const user = await this.userRepository.create(createUserDto);

    await this.userRepository.save(user);
    return user;
  }

  @ApiParam({ name: 'all' })
  async findAll() {
    return await this.userRepository.find({
      take: 100,
      order: { id: 'DESC' },
      select: { password: false },
    })
  }

  @ApiParam({ name: 'by id' })
  async findOne(id: number) {
    return await this.userRepository.findOne({
      where: { id },
      select: { password: false },
    });
  }

  @ApiBody({ type: UpdateUserDto })
  async update(id: number, updateUserDto: UpdateUserDto) {
    return await this.userRepository.update(id, updateUserDto);
  }

  @ApiParam( { name: 'by id' } )
  async remove(id: number) {
    const data = await this.userRepository.delete(id);
    return !!data.affected
  }

  /**
   * custom action
   */
  @ApiBody( { type: LoginUserDto })
  async login(payload: LoginUserDto) {
    const data = await this.userRepository.findOneBy( { email: payload.email })
    // user is found
    if (data) {
      const isMatch = await bcrypt.compare(payload.password, data.password);

      delete data.password;
      return isMatch ? { token: this.jwtService.sign(payload), user: data } : new UnauthorizedException();
    }
    
    return new UnauthorizedException();
  }

  async password(payload: UpdateUserPasswordDto) {
    const data = await this.userRepository.findOne({ where: { id: payload.id } })
    // user is found
    if (data) {
      const isMatch = await bcrypt.compare(payload.oldPassword, data.password);

      if (isMatch) {
        data.password = payload.password;
        return await this.userRepository.save(data);
      }      
    }

    return new BadRequestException();
  }
}
