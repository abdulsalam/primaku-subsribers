import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as bcrypt from 'bcrypt';
import { saltOrRounds } from 'src/config/constant';

@Injectable()
export class ParserMiddleware implements NestMiddleware {
    async use(req: Request, res: Response, next: NextFunction) {
        let payloadConverter = req.body;
        
        payloadConverter.password = await bcrypt.hash(payloadConverter.password, saltOrRounds);
        req.body = payloadConverter;
        next();
    }
}
