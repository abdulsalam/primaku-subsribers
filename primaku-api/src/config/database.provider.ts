import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../modules/users/entities/user.entity';
import { DataSource } from 'typeorm';
import { Configuration } from './constant';
import { UserTable1677509672568 } from '../database/migrations/1677509672568-UserTable';

export const databaseProvider = TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    inject: [ConfigService],
    // Use useFactory, useClass, or useExisting
    // to configure the DataSourceOptions.
    useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        entities: [User],        
        migrations: [UserTable1677509672568],
        synchronize: true,
        ...Configuration(configService),
    }),
    // dataSource receives the configured DataSourceOptions
    // and returns a Promise<DataSource>.
    dataSourceFactory: async (options) => {
        const dataSource = await new DataSource(options).initialize();
        
        // run migration
        await dataSource.runMigrations();

        return dataSource;
    },
});