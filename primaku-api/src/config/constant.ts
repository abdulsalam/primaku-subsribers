import { ConfigService } from "@nestjs/config"

/**
 * DB Config
 * @param configService 
 * @returns databaseConfigEnv
 */
export const Configuration = (configService: ConfigService) => {
    return {
        host: configService.get('DATABASE_HOST'),
        port: +configService.get('DATABASE_PORT') || 3306,
        username: configService.get('DATABASE_USER'),
        password: configService.get('DATABASE_PASSWORD'),
        database: configService.get('DATABASE_NAME'),
    }
}

/**
 * Attribute of databases
 */
export const TABLE = {
    USER: {
        NAME: 'users',
        COLUMNS: {
            ID: 'id',
            NAME: 'name',
            EMAIL: 'email',
            PASSWORD: 'password',
            ROLE: 'role',
        },
        MAXLENGTH: 255,
    }
}

/**
 * JWT Constants
 */
export const jwtConstants = {
    secret: 'OBcIwngfQpd7CXeOupxVuUO0RDjtTqcAtZzrGpCm',
};

/**
 * Salt
 */
export const saltOrRounds = 10;