import { InjectRepository } from '@nestjs/typeorm';
import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Server } from 'socket.io';
import { User } from '../modules/users/entities/user.entity';
import { Repository } from 'typeorm';

@WebSocketGateway(3005, {
  cors: {
    origin: '*',
  },
})
export class EventsGateway {
  @WebSocketServer()
  server: Server;

  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  @SubscribeMessage('eventMemberUsers')
  async findAll(@MessageBody() data: any): Promise<Observable<WsResponse<User>>> {
    const user: User[] = await this.userRepository.find({
      take: 100,
      order: { id: 'DESC' },
      select: { password: false }
    })
    return from(user).pipe(map(item => ({ event: 'eventMemberUsers', data: item })));
  }
}