# PrimaKu Subscription

A Client-Server App to consume user data via socket

## Tech and Tools
- NestJs (API)
- TypeORM (ORM Database)
- MySQL (Database)
- Socket.io (WebSocket)
- JWT Auth
- ReactJs (Client)
- MaterialUI (Design Framework)

## Directory
- primaku-api: contains API App
- primaku-client: contains Client App

## Result

- Login View
![login-view](assets/login-view.png)
- Login Wrong View
![wrong-login-view](assets/wrong-login-view.png)
- Dashboard View
![dashboard-view](assets/dashboard-view.png)
- Subscription of User View
![subscribe-view](assets/subscribe-view.png)

### Note
The app containerized using Docker, both for Client and API/Server.

**Please refer to corresponding directory** for further setup (detail of setup).

### Copyright
Abdul Salam (https://github.com/abdulsalam01/)